﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MandelbrotLibrary;
using System.Diagnostics;

namespace MandelbrotUnitTests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public unsafe void TestMandelnumber1()
        {
            int answer = -1;
            double x = 0.3;
            double y = 0.5;
            int max = 100;
            int actualAnswer = Fractal.getMandelNumber(&x, &y, &max);
            Assert.AreEqual(answer, actualAnswer);
        }

        [TestMethod]
        public unsafe void TestMandelnumber2()
        {
            int answer = 2;
            double x = 1;
            double y = 0.5;
            int max = 100;
            int actualAnswer = Fractal.getMandelNumber(&x, &y, &max);
            Assert.AreEqual(answer, actualAnswer);
        }
    }
}
