﻿using System;
using System.Drawing;

namespace MandelbrotLibrary
{
    unsafe public delegate int FractalMethod(double* x, double* y, int* maxIteration);
    unsafe public delegate Color ColouringMethod(int* fractalNumber);

    public unsafe class Fractal
    {
        FractalMap fractalMap_old = null;
        double midX_old = 0;
        double midY_old = 0;
        double scale_old = 0;
        int maxIteration_old = 0;
        int width_old = 0;
        int height_old = 0;
        FractalMethod FractalMethod_old = null;

        public FractalMap GetFractalMap(double midX, double midY, double scale, int maxIteration, int width, int height, FractalMethod getFractalNumber)
        {
            if (midX == midX_old && midY == midY_old && scale == scale_old && maxIteration==maxIteration_old && width == width_old && height == height_old && getFractalNumber == FractalMethod_old)
            {
                return fractalMap_old;
            }
            /* scale: size of area of fractal per pixel. Bigger scale: more zoomed out*/
            int[,] map = new int[width, height];
            double x_base = -(width / (double)2) * scale + midX;
            double y_base = -(height / (double)2) * scale + midY;
            double x_input = 0;
            double y_input = 0;

            for (int x = 0; x < width; x++)
            {
                x_input = x_base + scale * x;
                for (int y = 0; y < height; y++)
                {
                    y_input = y_base + scale * y;
                    map[x,y] = getFractalNumber(&x_input, &y_input, &maxIteration);
                }
            }
            midX_old = midX;
            midY_old = midY;
            scale_old = scale;
            maxIteration_old = maxIteration;
            width_old = width;
            height_old = height;
            FractalMethod_old = getFractalNumber;
            fractalMap_old = new FractalMap(map);
            return fractalMap_old;
        }

        public unsafe static int getMandelNumber(double* x, double* y, int* maxIteration)
        {
            int i = 0;
            double distance = 0.0;
            double a = 0.0;
            double b = 0.0;

            while (distance < 2.0)
            {
                IterateMandelbrot(x, y, &a, &b);
                distance = Math.Sqrt((a * a) + (b * b));
                i++;
                if (i == *maxIteration)
                {
                    return -1;
                    //when the distance to origin of a and b does not reach 2, it means that
                    //the pixel is inside the fractal, and therefore should get a different
                    //colour. -1 is passed on and coloured black in colouring. 
                }
            }
            return i;
        }
        public unsafe static int getInvertedMandelNumber(double* x, double* y, int* maxIteration)
        {
            int i = 0;
            double distance = 0.0;
            double a = 0.0;
            double b = 0.0;

            while (distance < 2.0)
            {
                IterateInvertedMandelbrot(x, y, &a, &b);
                distance = Math.Sqrt((a * a) + (b * b));
                i++;
                if (i == *maxIteration)
                {
                    return -1;
                }
            }
            return i;
        }

        public unsafe static int getOldMandelNumber(double* x, double* y, int* maxIteration)
        {
            int i = 0;
            double distance = 0.0;
            double a = 0.0;
            double b = 0.0;

            while (distance < 2.0)
            {
                IterateOldMandelbrot(x, y, &a, &b);
                distance = Math.Sqrt((a * a) + (b * b));
                i++;
                if (i == *maxIteration)
                {
                    return -1;
                }
            }
            return i;
        }
        public unsafe static int getInsaneMandelNumber(double* x, double* y, int* maxIteration)
        {
            int i = 0;
            double distance = 0.0;
            double a = 0.0;
            double b = 0.0;

            while (distance < 2.0)
            {
                IterateInsaneMandelbrot(x, y, &a, &b);
                distance = Math.Sqrt((a * a) + (b * b));
                i++;
                if (i == *maxIteration)
                {
                    return -1;
                }
            }
            return i;
        }

        private unsafe static void IterateMandelbrot(double* x, double* y, double* a, double* b)
        {
            double c = *a * *a - *b * *b + *x;
            *b = 2 * *a * *b + *y;
            *a = c;
        }
        private unsafe static void IterateInvertedMandelbrot(double* x, double* y, double* a, double* b)
        {
            double d = *a * *a * *a * *a + 2 * *a * *a * *b * *b  + *b * *b * *b * *b + *x;
            double c = (*a * *a - *b * *b) / d + *x;
            *b = -2 * *a * *b / d + *y;
            *a = c;
        }
        private unsafe static void IterateOldMandelbrot(double* x, double* y, double* a, double* b)
        {
            *a = *a * *a - *b * *b + *x;
            *b = 2 * *a * *b + *y;
        }
        private unsafe static void IterateInsaneMandelbrot(double* x, double* y, double* a, double* b)
        {
            double c = *a * *a - *b * *b + *x;
            *b = 1.5 * *b * *a + *y;
            *a = c;
        }
    }
}