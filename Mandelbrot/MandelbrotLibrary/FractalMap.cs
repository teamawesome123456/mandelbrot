﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MandelbrotLibrary
{
    public class FractalMap
    {
        int[,]map;

        public FractalMap(int[,] map) { 
            this.map = map;
        }

        public int[,] getNumberMap()
        {
            return map;
        }
    }
}
