﻿using System;
using System.Windows;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace MandelbrotLibrary
{
    unsafe public class Colouring
    {
        public static Color getFractalColor(int* fractalNumber, ColouringMethod Algorithm)
        {
            if (*fractalNumber == -1)
            {
                return Color.Black;
            }
            return Algorithm(fractalNumber);
        }

        //Here are methods wich return a method with build in colors so the caller only has the specify the colors once

        public static ColouringMethod BinaryAlgorithm(Color c, Color d)
        {
            Color ReturnAlgorithm(int* fractalNumber)
            {
                if (*fractalNumber % 2 == 0)
                {
                    return c;
                }
                else
                {
                    return d;
                }
            }
            return ReturnAlgorithm;
        }

        //These two methods precalculate the values for every possible fractalnumber-input so it can give back the results very quickly
        public static ColouringMethod LinearAlgorithm(Color c, bool lightness)
        {
            Color[] ColorMap = new Color[256];

            for (int i = 0; i < 256; i++)
            {
                Color result;
                double value = Math.Abs((((c.A + i) % 32) - 16) * 8);
                if (lightness)
                {
                    result = Color.FromArgb(255, (int)(255 - ((255 - c.R) * (value / 255))), (int)(255 - ((255 - c.G) * (value / 255))), (int)(255 - ((255 - c.B) * (value / 255))));
                }
                else
                {
                    result = Color.FromArgb(255, (int)(((c.R) * (value / 255))), (int)(((c.G) * (value / 255))), (int)(((c.B) * (value / 255))));
                }
                
                ColorMap[i] = result;
            }

            Color ReturnAlgorithm(int* fractalNumber)
            {
                return ColorMap[*fractalNumber % 128];
            }
            return ReturnAlgorithm;
        }

        //This method is unlike the others because it can not have specified colors
        public static ColouringMethod RadialAlgorithm()
        {
            Color[] ColorMap = new Color[128];

            for (int i = 0; i < 128; i++)
            {
                Color result;
                int relativeNumber = i * 2;
                if (relativeNumber < 64)
                {
                    result = Color.FromArgb(relativeNumber * 4, relativeNumber * 4, 255 - relativeNumber * 4);
                }
                else if (relativeNumber < 128)
                {
                    result = Color.FromArgb(255 - (relativeNumber - 64) * 4, 255, 0);
                }
                else if (relativeNumber < 192)
                {
                    result = Color.FromArgb(0, 255, (relativeNumber - 128) * 4);
                }
                else
                {
                    result = Color.FromArgb(0, 255 - ((relativeNumber - 192) * 4), 0);
                }
                ColorMap[i] = result;
            }

            Color ReturnAlgorithm(int* fractalNumber)
            {
                return ColorMap[*fractalNumber % 128];
            }
            return ReturnAlgorithm;
        }

        public static string ColorToString(Color col)
        {
            return col.ToString();
        }
    }
}
