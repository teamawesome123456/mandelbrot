﻿using MandelbrotLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;

namespace MandelbrotUI
{


    public unsafe partial class Form1 : Form
    {
        Fractal fractal;
        FractalMap fractalMap;
        ColouringMethod colouringAlgorithm;
        FractalMethod fractalAlgorithm;
        Bitmap currentBitmap;
        int width;
        int height;
        enum State { Done, Running, Colouring, Error };
        BindingList<Color> colorList;
        BindingList<Color> colorList2;

        public Form1()
        {
            InitializeComponent();

            //Initialize events
            fractalOutcomePictureBox.MouseDown += FractalOnMouseDown;
            fractalOutcomePictureBox.MouseUp += FractalOnMouseUp;
            fractalOutcomePictureBox.MouseWheel += zoom;
            typeListBox.SelectedValueChanged += updateColouringAlgorithm;
            fractalTypeListBox.SelectedValueChanged += updateFractalAlgorithm;
            calculateButton.Click += updateFractal;
            this.Resize += resize;

            //Initialize variables
            width = fractalOutcomePictureBox.Width;
            height = fractalOutcomePictureBox.Height;
            colouringAlgorithm = Colouring.BinaryAlgorithm(Color.White, Color.Black);
            fractal = new Fractal();
            fractalAlgorithm = Fractal.getMandelNumber;
            currentBitmap = new Bitmap(width, height);
            typeListBox.SelectedIndex = 0;
            fractalTypeListBox.SelectedIndex = 0;

            //Initialize color listboxes
            colorList = new BindingList<Color> { Color.Black, Color.White, Color.Red, Color.OrangeRed, Color.Orange, Color.Yellow, Color.YellowGreen, Color.Green, Color.Cyan, Color.Blue, Color.Purple, Color.Violet };
            colorList2 = new BindingList<Color>(colorList.ToList<Color>()); //Defined in this way because otherwise the list2 is bound to list1
            color1ListBox.DataSource = colorList;
            color2ListBox.DataSource = colorList2;
            colouringMethodListBox.SelectedIndex = 0;
            colouringMethodListBox.SelectedIndexChanged += UpdateColouringAlgorithmUserInput;
            color1ListBox.SelectedIndexChanged += UpdateColouringAlgorithmUserInput;
            color2ListBox.SelectedIndexChanged += UpdateColouringAlgorithmUserInput;
            colouringMethodListBox.Enabled = false;
            color1ListBox.Enabled = false;
            color2ListBox.Enabled = false;

            //Draw the fractal
            updateFractal();

            Debug.WriteLine("This program will give debug output: the time running in the most used functions (all time is in mileseconds (ms))");
        }

        private unsafe void updateFractal(Object o = null, EventArgs ea = null)
        {
            Debug.WriteLine("    New calculate sequence");
            var totalStopWatch = Stopwatch.StartNew();
            updateStateLabel(State.Running);

            double midX, midY, scale;
            int maxIteration;

            try
            {
                midX = Convert.ToDouble(midXTextBox.Text);
                midY = Convert.ToDouble(midYTextBox.Text);
                scale = Convert.ToDouble(scaleTextBox.Text);
                maxIteration = Convert.ToInt32(maxTextBox.Text);
            }
            catch
            {
                MessageBox.Show("One of the values you entered is not valid.");
                updateStateLabel(State.Error);
                return;
            }

            var calculationsStopWatch = Stopwatch.StartNew();

            fractalMap = fractal.GetFractalMap(midX, midY, scale, maxIteration, width, height, fractalAlgorithm);

            calculationsStopWatch.Stop();
            Debug.WriteLine("Calculating:\t" + calculationsStopWatch.ElapsedMilliseconds);

            updateColours();

            totalStopWatch.Stop();
            Debug.WriteLine("Whole function:\t" + totalStopWatch.ElapsedMilliseconds);
        }

        private void updateColours()
        {
            //Updates the colors based on the last calculated fractalnumbers (so it can be fast in changing the colors of an already drawn fractal)
            var colouringStopWatch = Stopwatch.StartNew();
            updateStateLabel(State.Colouring);

            if (fractalMap == null)
            {
                //Make sure there is a fractalmap to draw
                updateFractal();
            }
            currentBitmap = GetFractalBitmap(fractalMap, colouringAlgorithm);
            fractalOutcomePictureBox.Image = currentBitmap;

            colouringStopWatch.Stop();
            Debug.WriteLine("Colouring:\t" + colouringStopWatch.ElapsedMilliseconds);
            updateStateLabel(State.Done);
        }

        unsafe public Bitmap GetFractalBitmap(FractalMap map, ColouringMethod ColorFunction)
        {
            //The directbitmap is faster in getting and setting pixels because 
            DirectBitmap output = new DirectBitmap(width, height);
            int[,] numberMap = map.getNumberMap();

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int temp2 = numberMap[x, y];
                    Color temp = Colouring.getFractalColor(&temp2, ColorFunction);
                    output.Bits[x + y * width] = temp.A * 16777216 + temp.R * 65536 + temp.G * 256 + temp.B;
                }
            }
            return output.Bitmap;
        }

        private void updateColouringAlgorithm(Object sender, EventArgs e)
        {
            colouringMethodListBox.Enabled = false;
            color1ListBox.Enabled = false;
            color2ListBox.Enabled = false;
            switch (typeListBox.SelectedItem.ToString())
            {
                case "Basis":
                    colouringAlgorithm = Colouring.BinaryAlgorithm(Color.Azure, Color.Black);
                    break;
                case "Zuilen":
                    colouringAlgorithm = Colouring.LinearAlgorithm(Color.SaddleBrown, true);
                    break;
                case "Vuur":
                    colouringAlgorithm = Colouring.LinearAlgorithm(Color.Red, false);
                    break;
                case "Zigzag":
                    colouringAlgorithm = Colouring.RadialAlgorithm();
                    break;
                case "Custom":
                    colouringMethodListBox.Enabled = true;
                    UpdateColouringAlgorithmUserInput(sender, e);
                    break;
                default:
                    colouringAlgorithm = Colouring.BinaryAlgorithm(Color.Orange, Color.Black);
                    break;
            }
            updateColours();
        }

        private void updateFractalAlgorithm(Object sender, EventArgs e)
        {
            fractalTypeListBox.SelectedItem.ToString();
            switch (fractalTypeListBox.SelectedItem.ToString())
            {
                case "Mandelbrot":
                    fractalAlgorithm = Fractal.getMandelNumber;
                    break;
                case "Mandelbrot^-2":
                    fractalAlgorithm = Fractal.getInvertedMandelNumber;
                    break;
                case "Our First Fractal":
                    fractalAlgorithm = Fractal.getOldMandelNumber;
                    break;
                case "Insane Mandel":
                    fractalAlgorithm = Fractal.getInsaneMandelNumber;
                    break;
                default:
                    fractalAlgorithm = Fractal.getMandelNumber;
                    break;
            }
            updateFractal();
        }

        private void updateStateLabel(State state)
        {
            switch (state)
            {
                case State.Running:
                    stateLabel.Text = "Calculating...";
                    break;
                case State.Colouring:
                    stateLabel.Text = "Colouring...";
                    break;
                case State.Done:
                    stateLabel.Text = "";
                    break;
                case State.Error:
                    stateLabel.Text = "Error";
                    break;
                default:
                    stateLabel.Text = "None";
                    break;
            }
            stateLabel.Refresh();
        }

        private void exportButton_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog();
            dialog.Filter = "All image formats|*.png;*.jpg;*.tiff;*.gif;*.bmp;*.emf;*.wmf|PNG|*.png|JPG|*.jpg|TIFF|*.tiff|GIF|*.gif|BMP|*.bmp|Enhanced metafile (EMF)|*.emf|Windows metafile(WMF)|*.wmf";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string filename = dialog.FileName;
                string extension = Path.GetExtension(filename);
                ImageFormat imageFormat = ImageFormat.Bmp;

                switch (extension)
                {
                    case ".png":
                        imageFormat = ImageFormat.Png;
                        break;
                    case ".jpg":
                        imageFormat = ImageFormat.Jpeg;
                        break;
                    case ".tiff":
                        imageFormat = ImageFormat.Tiff;
                        break;
                    case ".gif":
                        imageFormat = ImageFormat.Gif;
                        break;
                    case ".bmp":
                        imageFormat = ImageFormat.Bmp;
                        break;
                    case ".emf":
                        imageFormat = ImageFormat.Emf;
                        break;
                    case ".wmf":
                        imageFormat = ImageFormat.Wmf;
                        break;
                }

                currentBitmap.Save(dialog.FileName, imageFormat);
            }
        }

        private void resize(object sender, EventArgs e)
        {
            Size newSize = this.ClientSize;
            int temp = newSize.Width - UIpanel.Size.Width;

            fractalOutcomePictureBox.Size = new Size(temp, newSize.Height);
            UIpanel.Location = new Point(temp, 0);

            width = fractalOutcomePictureBox.Width;
            height = fractalOutcomePictureBox.Height;

            fractalMap = null; //Invalidate the last calculations

            updateFractal();
        }

        //Panning
        double start_x;
        double start_y;

        protected void FractalOnMouseDown(object o, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;

            start_x = (double)e.X / width - 0.5 * width * Convert.ToDouble(scaleTextBox.Text);
            start_y = (double)e.Y / height - 0.5 * height * Convert.ToDouble(scaleTextBox.Text);
        }

        protected void FractalOnMouseUp(object o, MouseEventArgs e)
        {
            Cursor = Cursors.Default;

            double factor = 1000 * Convert.ToDouble(scaleTextBox.Text);
            double end_x = (double)e.X / width - 0.5 * width * Convert.ToDouble(scaleTextBox.Text);
            double end_y = (double)e.Y / height - 0.5 * height * Convert.ToDouble(scaleTextBox.Text);
            double delta_x = end_x - start_x;
            double delta_y = end_y - start_y;

            midXTextBox.Text = (Convert.ToDouble(midXTextBox.Text) - delta_x * factor).ToString();
            midYTextBox.Text = (Convert.ToDouble(midYTextBox.Text) - delta_y * factor).ToString();

            updateFractal();
        }

        //Zooming
        private void zoom(object sender, MouseEventArgs mea)
        {
            try
            {
                double delta = 1 - mea.Delta / (double)600;

                if (Convert.ToDouble(scaleTextBox.Text) != 0)
                {
                    scaleTextBox.Text = (Convert.ToDouble(scaleTextBox.Text) * delta).ToString();
                }
                else
                {
                    scaleTextBox.Text = (0.01 * delta).ToString();
                }

                updateFractal();
            }
            catch
            {
                MessageBox.Show("One of the values you entered is not valid.");
                updateStateLabel(State.Error);
                return;
            }
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            midXTextBox.Text = "0";
            midYTextBox.Text = "0";
            scaleTextBox.Text = "0.01";
            maxTextBox.Text = "100";
            typeListBox.SelectedIndex = 0;
            fractalTypeListBox.SelectedIndex = 0;
            colouringMethodListBox.Enabled = false;
            color1ListBox.Enabled = false;
            color2ListBox.Enabled = false;

            updateFractal();
        }

        private void UpdateColouringAlgorithmUserInput(object o, EventArgs ea)
        {
            switch (colouringMethodListBox.SelectedItem)
            {
                case "Binary":
                    colouringAlgorithm = Colouring.BinaryAlgorithm((Color)color1ListBox.SelectedItem, (Color)color2ListBox.SelectedItem);
                    color1ListBox.Enabled = true;
                    color2ListBox.Enabled = true;
                    break;
                case "Linear Light":
                    colouringAlgorithm = Colouring.LinearAlgorithm((Color)color1ListBox.SelectedItem, true);
                    color1ListBox.Enabled = true;
                    color2ListBox.Enabled = false;
                    break;
                case "Linear Dark":
                    colouringAlgorithm = Colouring.LinearAlgorithm((Color)color1ListBox.SelectedItem, false);
                    color1ListBox.Enabled = true;
                    color2ListBox.Enabled = false;
                    break;
            }
            updateFractal();
        }

        private void addColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                colorList.Add(colorDialog.Color);
                colorList2.Add(colorDialog.Color);
            }
        }
    }
}
