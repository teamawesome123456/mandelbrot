﻿namespace MandelbrotUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.calculateButton = new System.Windows.Forms.Button();
            this.midXLabel = new System.Windows.Forms.Label();
            this.midXTextBox = new System.Windows.Forms.TextBox();
            this.fractalOutcomePictureBox = new System.Windows.Forms.PictureBox();
            this.typeListBox = new System.Windows.Forms.ListBox();
            this.midYTextBox = new System.Windows.Forms.TextBox();
            this.midYLabel = new System.Windows.Forms.Label();
            this.scaleTextBox = new System.Windows.Forms.TextBox();
            this.scaleLabel = new System.Windows.Forms.Label();
            this.maxTextBox = new System.Windows.Forms.TextBox();
            this.maxLabel = new System.Windows.Forms.Label();
            this.stateLabel = new System.Windows.Forms.Label();
            this.exportButton = new System.Windows.Forms.Button();
            this.UIpanel = new System.Windows.Forms.Panel();
            this.addColorButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.color2ListBox = new System.Windows.Forms.ListBox();
            this.color1ListBox = new System.Windows.Forms.ListBox();
            this.colouringMethodListBox = new System.Windows.Forms.ListBox();
            this.fractalTypeListBox = new System.Windows.Forms.ListBox();
            this.resetButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.fractalOutcomePictureBox)).BeginInit();
            this.UIpanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // calculateButton
            // 
            this.calculateButton.Location = new System.Drawing.Point(20, 232);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(158, 23);
            this.calculateButton.TabIndex = 0;
            this.calculateButton.Text = "OK";
            this.calculateButton.UseVisualStyleBackColor = true;
            // 
            // midXLabel
            // 
            this.midXLabel.AutoSize = true;
            this.midXLabel.Location = new System.Drawing.Point(17, 34);
            this.midXLabel.Name = "midXLabel";
            this.midXLabel.Size = new System.Drawing.Size(55, 13);
            this.midXLabel.TabIndex = 1;
            this.midXLabel.Text = "Midden X:";
            // 
            // midXTextBox
            // 
            this.midXTextBox.Location = new System.Drawing.Point(78, 31);
            this.midXTextBox.Name = "midXTextBox";
            this.midXTextBox.Size = new System.Drawing.Size(100, 20);
            this.midXTextBox.TabIndex = 2;
            this.midXTextBox.Text = "0";
            // 
            // fractalOutcomePictureBox
            // 
            this.fractalOutcomePictureBox.Location = new System.Drawing.Point(0, 0);
            this.fractalOutcomePictureBox.Name = "fractalOutcomePictureBox";
            this.fractalOutcomePictureBox.Size = new System.Drawing.Size(1000, 1000);
            this.fractalOutcomePictureBox.TabIndex = 3;
            this.fractalOutcomePictureBox.TabStop = false;
            // 
            // typeListBox
            // 
            this.typeListBox.FormattingEnabled = true;
            this.typeListBox.Items.AddRange(new object[] {
            "Basis",
            "Zuilen",
            "Vuur",
            "Zigzag",
            "Custom"});
            this.typeListBox.Location = new System.Drawing.Point(20, 157);
            this.typeListBox.Name = "typeListBox";
            this.typeListBox.Size = new System.Drawing.Size(67, 69);
            this.typeListBox.TabIndex = 4;
            // 
            // midYTextBox
            // 
            this.midYTextBox.Location = new System.Drawing.Point(78, 57);
            this.midYTextBox.Name = "midYTextBox";
            this.midYTextBox.Size = new System.Drawing.Size(100, 20);
            this.midYTextBox.TabIndex = 6;
            this.midYTextBox.Text = "0";
            // 
            // midYLabel
            // 
            this.midYLabel.AutoSize = true;
            this.midYLabel.Location = new System.Drawing.Point(17, 60);
            this.midYLabel.Name = "midYLabel";
            this.midYLabel.Size = new System.Drawing.Size(55, 13);
            this.midYLabel.TabIndex = 5;
            this.midYLabel.Text = "Midden Y:";
            // 
            // scaleTextBox
            // 
            this.scaleTextBox.Location = new System.Drawing.Point(78, 83);
            this.scaleTextBox.Name = "scaleTextBox";
            this.scaleTextBox.Size = new System.Drawing.Size(100, 20);
            this.scaleTextBox.TabIndex = 8;
            this.scaleTextBox.Text = "0.01";
            // 
            // scaleLabel
            // 
            this.scaleLabel.AutoSize = true;
            this.scaleLabel.Location = new System.Drawing.Point(17, 86);
            this.scaleLabel.Name = "scaleLabel";
            this.scaleLabel.Size = new System.Drawing.Size(43, 13);
            this.scaleLabel.TabIndex = 7;
            this.scaleLabel.Text = "Schaal:";
            // 
            // maxTextBox
            // 
            this.maxTextBox.Location = new System.Drawing.Point(78, 112);
            this.maxTextBox.Name = "maxTextBox";
            this.maxTextBox.Size = new System.Drawing.Size(100, 20);
            this.maxTextBox.TabIndex = 10;
            this.maxTextBox.Text = "100";
            // 
            // maxLabel
            // 
            this.maxLabel.AutoSize = true;
            this.maxLabel.Location = new System.Drawing.Point(17, 112);
            this.maxLabel.Name = "maxLabel";
            this.maxLabel.Size = new System.Drawing.Size(30, 13);
            this.maxLabel.TabIndex = 9;
            this.maxLabel.Text = "Max:";
            // 
            // stateLabel
            // 
            this.stateLabel.AutoSize = true;
            this.stateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.stateLabel.ForeColor = System.Drawing.Color.Red;
            this.stateLabel.Location = new System.Drawing.Point(15, 320);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(63, 26);
            this.stateLabel.TabIndex = 11;
            this.stateLabel.Text = "State";
            // 
            // exportButton
            // 
            this.exportButton.Location = new System.Drawing.Point(20, 261);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(158, 23);
            this.exportButton.TabIndex = 12;
            this.exportButton.Text = "Export";
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
            // 
            // UIpanel
            // 
            this.UIpanel.Controls.Add(this.addColorButton);
            this.UIpanel.Controls.Add(this.label7);
            this.UIpanel.Controls.Add(this.label6);
            this.UIpanel.Controls.Add(this.label5);
            this.UIpanel.Controls.Add(this.label4);
            this.UIpanel.Controls.Add(this.label3);
            this.UIpanel.Controls.Add(this.label2);
            this.UIpanel.Controls.Add(this.label1);
            this.UIpanel.Controls.Add(this.color2ListBox);
            this.UIpanel.Controls.Add(this.color1ListBox);
            this.UIpanel.Controls.Add(this.colouringMethodListBox);
            this.UIpanel.Controls.Add(this.fractalTypeListBox);
            this.UIpanel.Controls.Add(this.resetButton);
            this.UIpanel.Controls.Add(this.midXTextBox);
            this.UIpanel.Controls.Add(this.exportButton);
            this.UIpanel.Controls.Add(this.calculateButton);
            this.UIpanel.Controls.Add(this.stateLabel);
            this.UIpanel.Controls.Add(this.midXLabel);
            this.UIpanel.Controls.Add(this.maxTextBox);
            this.UIpanel.Controls.Add(this.typeListBox);
            this.UIpanel.Controls.Add(this.maxLabel);
            this.UIpanel.Controls.Add(this.midYLabel);
            this.UIpanel.Controls.Add(this.scaleTextBox);
            this.UIpanel.Controls.Add(this.midYTextBox);
            this.UIpanel.Controls.Add(this.scaleLabel);
            this.UIpanel.Location = new System.Drawing.Point(1000, 0);
            this.UIpanel.Name = "UIpanel";
            this.UIpanel.Size = new System.Drawing.Size(200, 707);
            this.UIpanel.TabIndex = 13;
            // 
            // addColorButton
            // 
            this.addColorButton.Location = new System.Drawing.Point(20, 658);
            this.addColorButton.Name = "addColorButton";
            this.addColorButton.Size = new System.Drawing.Size(158, 23);
            this.addColorButton.TabIndex = 14;
            this.addColorButton.Text = "Add Color";
            this.addColorButton.UseVisualStyleBackColor = true;
            this.addColorButton.Click += new System.EventHandler(this.addColorButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Parameters";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 538);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Color 2:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 424);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Color 1:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 346);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Custom Colouring";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 362);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Colouring Algorithm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(90, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Fractal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Colouring";
            // 
            // color2ListBox
            // 
            this.color2ListBox.FormattingEnabled = true;
            this.color2ListBox.Location = new System.Drawing.Point(20, 557);
            this.color2ListBox.Name = "color2ListBox";
            this.color2ListBox.Size = new System.Drawing.Size(158, 95);
            this.color2ListBox.TabIndex = 17;
            // 
            // color1ListBox
            // 
            this.color1ListBox.FormattingEnabled = true;
            this.color1ListBox.Location = new System.Drawing.Point(20, 440);
            this.color1ListBox.Name = "color1ListBox";
            this.color1ListBox.Size = new System.Drawing.Size(158, 95);
            this.color1ListBox.TabIndex = 16;
            // 
            // colouringMethodListBox
            // 
            this.colouringMethodListBox.FormattingEnabled = true;
            this.colouringMethodListBox.Items.AddRange(new object[] {
            "Binary",
            "Linear Light",
            "Linear Dark"});
            this.colouringMethodListBox.Location = new System.Drawing.Point(20, 378);
            this.colouringMethodListBox.Name = "colouringMethodListBox";
            this.colouringMethodListBox.Size = new System.Drawing.Size(158, 43);
            this.colouringMethodListBox.TabIndex = 15;
            // 
            // fractalTypeListBox
            // 
            this.fractalTypeListBox.FormattingEnabled = true;
            this.fractalTypeListBox.Items.AddRange(new object[] {
            "Mandelbrot",
            "Mandelbrot^-2",
            "Our First Fractal",
            "Insane Mandel"});
            this.fractalTypeListBox.Location = new System.Drawing.Point(93, 157);
            this.fractalTypeListBox.Name = "fractalTypeListBox";
            this.fractalTypeListBox.Size = new System.Drawing.Size(85, 69);
            this.fractalTypeListBox.TabIndex = 14;
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(20, 290);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(158, 23);
            this.resetButton.TabIndex = 13;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.calculateButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1202, 741);
            this.Controls.Add(this.UIpanel);
            this.Controls.Add(this.fractalOutcomePictureBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Mandelbrot";
            ((System.ComponentModel.ISupportInitialize)(this.fractalOutcomePictureBox)).EndInit();
            this.UIpanel.ResumeLayout(false);
            this.UIpanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label midXLabel;
        private System.Windows.Forms.TextBox midXTextBox;
        private System.Windows.Forms.PictureBox fractalOutcomePictureBox;
        private System.Windows.Forms.ListBox typeListBox;
        private System.Windows.Forms.TextBox midYTextBox;
        private System.Windows.Forms.Label midYLabel;
        private System.Windows.Forms.TextBox scaleTextBox;
        private System.Windows.Forms.Label scaleLabel;
        private System.Windows.Forms.TextBox maxTextBox;
        private System.Windows.Forms.Label maxLabel;
        private System.Windows.Forms.Label stateLabel;
        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.Panel UIpanel;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.ListBox fractalTypeListBox;
        private System.Windows.Forms.ListBox colouringMethodListBox;
        private System.Windows.Forms.ListBox color2ListBox;
        private System.Windows.Forms.ListBox color1ListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button addColorButton;
    }
}

